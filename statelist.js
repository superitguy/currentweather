
let states =['AB', 'AK', 'AL', 'AR', 'AS', 'AZ', 'BC', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'GU', 'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MB', 'MD', 'ME', 'MI', 'MN', 'MO', 'MP', 'MS', 'MT', 'NB', 'NC', 'ND', 'NE', 'NF', 'NH', 'NJ', 'NM', 'NS', 'NT', 'NU', 'NV', 'NY', 'OH', 'OK', 'ON', 'OR', 'PA', 'PE', 'PR', 'QC', 'RI', 'SC', 'SD', 'SK', 'TN', 'TX', 'UT', 'VA', 'VI', 'VT', 'WA', 'WI', 'WV', 'WY', 'YT']
    .join(', ').trim() ;
var readlineSync = require('readline-sync');
var stationinfo= require('./stationinfo');
var capitalfirst=require('./capitalfirst');
const request= require('request');
const xml2js= require('xml2js');

function findStations(abr, stations) {


    let searchResults= [];




    for ( n=0; n<stations.length; n++) {
        const station = stations[n];
        if (station.state[0]==abr) {

            searchResults.push(station);
        }
    }



    let stationnames=[];
    for ( v=0; v<searchResults.length; v++) {
        stationnames.push(searchResults[v].station_name[0]);

    }


    let userStation = readlineSync.question('Please choose a sizable city you would like to know the current weather in (if city has multiple words in it, only type the most distinct one, e.g. for San Francisco, just enter: Francisco) : ');
    //change first letter to capital
let finalStation= capitalfirst(userStation);

//get number in array of available station
let yourStation= stationinfo(finalStation,stationnames);
console.log(yourStation);
//weatherstation url
let   url= [];


    for ( r=0; r<stations.length; r++) {
        const station = stations[r];
        if (station.station_name[0]==yourStation) {

            url.push(station.rss_url);

        }

    }

    let boom=JSON.stringify(url[0]);



    var myUrl2 = boom.replace("[", "");
    var myUrl3= myUrl2.replace("]", "");


    var finalWeather = myUrl3;
    var myfinalWeather= finalWeather.replace(/["]+/g, "");


   let aweatherStationUrl=myfinalWeather;
    let aoptions;
    aoptions = {
        url: aweatherStationUrl,
        headers: {
            'User-Agent': 'arequest'
        }
    };

    request(aoptions,(error,response,body)=> {

        output(body);

    });
    function output(rawString) {
        var parseString = require('xml2js').parseString;

        parseString(rawString, function (err, result) {


            console.log("Your current weather is "+result.rss.channel[0].item[0].title[0]);

        });
    }
}
module.exports =findStations;
